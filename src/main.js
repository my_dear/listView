import Vue from 'vue';      //引入 vue 实例
import AppView from './App.vue';    //引入 App.vue 文件
import router from './router';
import axios from 'axios';          //引入 axios

import ElementUI from 'element-ui';     //引入 element-ui
import 'element-ui/lib/theme-default/index.css';    // 默认主题

// import '../static/css/theme-green/index.css';       // 浅绿色主题

Vue.use(ElementUI);
Vue.prototype.$http = axios;// 把 axios 请求方法挂载到 Vue 原型的 $http上

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (USER === null ) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // 确保一定要调用 next()
  }
})

new Vue({
    router: router,
    render: h => h(AppView)
}).$mount('#app');
