import axios from 'axios'

export default {
    get (url, data = null) {
        if (!url) {
            console.error('API function call requires url argument')
            return
        }
        return axios.get(OPEN_API + url + SUBJOIN_PARA, data)
    },

    post (url, data = null){
        if (!url) {
            console.error('API function call requires url argument')
            return
        }
        return axios.post(OPEN_API + url + SUBJOIN_PARA, data)
    },

    byNumGetObj:function (t,ts){
        ts = ts||USER_ROLE_TYPE;
        for (var i=0; i<ts.length; i++){
            if (t==ts[i].t){
                return ts[i];
            }
        }
    }
}
