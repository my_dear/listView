import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    // mode: 'history',
    routes: [
        {
            path: '/',
            meta: { requiresAuth: true },
            component: resolve => require(['../components/common/Home.vue'], resolve),
            children:[
                {
                    path: '/',
                    // meta: { requiresAuth: true },
                    component: resolve => require(['../components/page/UserList.vue'], resolve)
                },
                {
                    path: '/userlist',
                    // meta: { requiresAuth: true },
                    component: resolve => require(['../components/page/UserList.vue'], resolve)
                },
                {
                    path: '/basetable',
                    component: resolve => require(['../components/page/BaseTable.vue'], resolve)
                },
                {
                    path: '/vuetable',
                    component: resolve => require(['../components/page/VueTable.vue'], resolve)     // vue-datasource组件
                },
                {
                    path: '/baseform',
                    component: resolve => require(['../components/page/BaseForm.vue'], resolve)
                },
                {
                    path: '/vueeditor',
                    component: resolve => require(['../components/page/VueEditor.vue'], resolve)    // Vue-Quill-Editor组件
                },
                {
                    path: '/markdown',
                    component: resolve => require(['../components/page/Markdown.vue'], resolve)     // Vue-Quill-Editor组件
                },
                {
                    path: '/upload',
                    component: resolve => require(['../components/page/Upload.vue'], resolve)       // Vue-Core-Image-Upload组件
                },
                {
                    path: '/basecharts',
                    component: resolve => require(['../components/page/BaseCharts.vue'], resolve)   // vue-schart组件
                },
                {
                    path: '/drag',
                    component: resolve => require(['../components/page/DragList.vue'], resolve)    // 拖拽列表组件
                },
                {
                    path: '/useradd',
                        component: resolve => require(['../components/page/UserEditAdd.vue'], resolve)    // 添加用户
                },
                {
                    path: '/productlist',
                        component: resolve => require(['../components/page/ProductList.vue'], resolve)    // 商品列表
                },
                {
                    path: '/oderlist',
                        component: resolve => require(['../components/page/OderList.vue'], resolve)    // 订单列表
                },
                {
                    name: 'userdetail',
                    path: '/userdetail',
                        component: resolve => require(['../components/page/UserEditUser.vue'], resolve)    // 用户详情
                }
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
    ]
})
