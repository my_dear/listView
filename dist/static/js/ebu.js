var COMPANY = {
	title:"云制造 - 快时尚柔性供应链专家",
	name:"云制造",
	summary:"快时尚柔性供应链专家",
	footer:"Copyright&copy;2016-2017 广州市云制造信息科技有限公司 yunzhizaocn.com All Rights Reserved. 备案号：粤ICP备16027249号",
	appName:7,
	copyright:"Copyright&copy;2016-2017",
	company:"云制造信息科技有限公司",
	record:"备案号：粤ICP备16027249号"
};

var USER = getUser();

var OPEN_API = "/drpapi";
var SUBJOIN_PARA = "?appName=" + COMPANY.appName + (USER?"&userId="+USER.userId||"":"");

function getUser(){
    return JSON.parse(sessionStorage.getItem("USER"));
}

//	{t: 0, n: "全部类型", s: "ALL"},
var USER_ROLE_TYPE = [
	{t: 32,  n: "代理商", s: "SHOPPER"},
	{t: 100, n: "注册用户", s: "USER"},
	{t: 16,  n: "市场管理员", s: "MARKET"},
	{t: 3,   n: "超级管理员", s: "ADMIN"},
	{t: 17,  n: "运营管理员", s: "OPERATION"},
	{t: 18,  n: "财务管理员", s: "FINANCE"},
	{t: 19,  n: "跟单管理员", s: "DOCUMENTARY"}
];

var ORDER_STATUS = [
    {t:-1,n:"全部",s:"ALL"},
    {t:0,n:"待付款",s:"ARREARAGE"},
    {t:1,n:"待处理",s:"PENDING"},
    {t:5,n:"已处理",s:"HANDLED"},
    {t:2,n:"已发货",s:"DELIVERED"},
    {t:3,n:"已收货",s:"RECEIVED"},
    {t:4,n:"已取消",s:"CANCELED"}
];

var PRODUCT_STATUS=[
	{t:4,n:"待审核",s:'PANDING'},
	{t:1,n:"出售中",s:'SALE'},
	{t:2,n:"已下架",s:'SOLD'},
	{t:3,n:"不通过",s:'NOPASS'},
	{t:5,n:"待编辑",s:'EDIT'}
];
